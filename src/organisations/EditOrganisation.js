import axios from "axios";
import { useEffect, useState } from "react";
import { Link,useNavigate, useParams } from "react-router-dom";

const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

export default function EditOrganisation() {
  const { id } = useParams();

  let navigate = useNavigate();

  const [organisation, setOrganisation] = useState({
    name: "",
    address: "",
  });

  const onInputChange = (e) => {
    setOrganisation({ ...organisation, [e.target.name]: e.target.value });
};

  const { address } = organisation;
  
  useEffect(() => {
    loadOrganisation();
  }, [id]);

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:8080/organisation/update/${id}`, organisation);
    navigate("/organisation/readAll");
  };

  const loadOrganisation = async () => {
    const result = await axios.get(
      `http://localhost:8080/organisation/getId/${id}`,
      axiosConfig
    );
    setOrganisation(result.data);
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Edit Organisation</h2>
          <form onSubmit={onSubmit}>
            {/* <div className="mb-3">
              <label htmlFor="Name" className="form-label">
                Name
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your name"
                name="name"
                value={name}
                onChange={onInputChange}
              />
            </div> */}
            <div className="mb-3">
              <label htmlFor="Address" className="form-label">
              Address
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your address"
                name="address"
                value={address}
                onChange={onInputChange}
              />
            </div>
            <button type="submit" className="btn btn-success">
              Edit
            </button>
            <Link className="btn btn-danger mx-2" to={"/organisation/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
