import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import "./AddOrganisation";

export default function AddOrganisation() {
  let navigate = useNavigate();

  const [organisation, setOrganisation] = useState({
    name: "",
    address: "",
  });

  const { name, address } = organisation;

  const [errorsForm, setErrorsForm] = useState({});

  const onInputChange = (e) => {
    const target = e.target;
    setOrganisation({ ...organisation, [target.name]: target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    const errors = validate(organisation);
    setErrorsForm(errors);
    console.log(e);
    await axios.post("http://localhost:8080/organisation/create", organisation);
    navigate("/organisation/readAll");
  };

  const validate = (values) => {
    const errors = {};
    console.log(values)
    if (!values.name) {
      errors.name = "Name cannot be empty!";
    } else if (values.name.length < 3) {
      errors.name = "Name must be more than 3 characters!";
    } else if (values.name.length > 30) {
        errors.name="Name must be less than 30 characters!"
    }
  
    if(!values.address){
        errors.address="Address cannot be empty!"
    }else if(values.address.length < 5){
        errors.address="Address must be more than 5 characters!"
    }else if (values.address.length > 50) {
        errors.address="Address must be less than 50 characters!"
    }
    return errors;
  };


  return (
    <div className="container mt-4">
    <div className="row">
      <div className="col-md-6 offset-md-3  p-4 mt-4 shadow">
        <h2 className="text-center m-4">Register Organisation</h2>
        <form onSubmit={onSubmit}>
          <div className="mb-3">
            <label htmlFor="Name" className="form-label">
              Name
            </label>
            <input
              type={"text"}
              className="form-control"
              placeholder="Enter your name!"
              name="name"
              value={name}
              onChange={onInputChange}
            />
          </div>
          <p className="error-message">{errorsForm.name}</p>
          <div className="mb-3">
            <label htmlFor="Address" className="form-label">
            Address
            </label>
            <input
              type={"text"}
              className="form-control"
              placeholder="Enter your address!"
              name="address"
              value={address}
              onChange={onInputChange}
            />
          </div>
          <p className="error-message">{errorsForm.address}</p>
          <button type="submit" className="btn btn-success">
            Register
          </button>
          <Link className="btn btn-danger mx-2" to={"/organisation/readAll"}>
            Cancel
          </Link>
        </form>
      </div>
    </div>
  </div>
);
}
