import axios from "axios";
import { useEffect, useState } from "react";
import { Link,useNavigate, useParams } from "react-router-dom";

const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

export default function EditDepatment() {
  const { id } = useParams();

  let navigate = useNavigate();

  const [department, setDepartment] = useState({
    name: "",
    employeesCount: "",
  });

  const { employeesCount } = department;

  const onInputChange = (e) => {
    setDepartment({ ...department, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadDepartment();
  }, [id]);

  const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
    onInputChange(e);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:8080/department/update/${id}`, department);
    navigate("/department/readAll");
  };

  const loadDepartment = async () => {
    const result = await axios.get(
      `http://localhost:8080/department/getId/${id}`,
      axiosConfig
    );
    setDepartment(result.data);
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Edit Department</h2>
          <form onSubmit={onSubmit}>
            {/* <div className="mb-3">
              <label htmlFor="Name" className="form-label">
                Name
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your name"
                name="name"
                value={name}
                onChange={onInputChange}
              />
            </div> */}
            <div className="mb-3">
              <label htmlFor="Employees Count" className="form-label">
              Employees count
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your employees count!"
                name="employeesCount"
                value={employeesCount}
                onChange={handleNumberInputChange}
              />
            </div>
            <button type="submit" className="btn btn-success">
              Edit
            </button>
            <Link className="btn btn-danger mx-2" to={"/department/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
