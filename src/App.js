import React from "react";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./layout/Navbar";
import Home from "./pages/Home";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AddUser from "./users/AddUser";
import EditUser from "./users/EditUser";
import ViewUser from "./users/ViewUser";
import ViewRole from "./roles/ViewRole";
import EditRole from "./roles/EditRole";
import AddRole from "./roles/AddRole";
import AllUsers from "./users/AllUsers";
import AllRoles from "./roles/AllRoles";
import AllDepartments from "./departments/AllDepartments";
import AllOrganisations from "./organisations/AllOrganisations";
import AddDepartment from "./departments/AddDepartment";
import AddOrganisation from "./organisations/AddOrganisation";
import EditOrganisation from "./organisations/EditOrganisation";
import EditDepartment from "./departments/EditDepartment";
import AddToUserNewRole from "./users/AddToUserNewRole"
import RemoveRole from "./users/RemoveRole";

function App() {

  return (
    <div className="App">
      <Router>
        <Navbar />
  
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route
            exact
            path="/organisation/readAll"
            element={<AllOrganisations />}
          />
          <Route
            exact
            path="/organisation/create"
            element={<AddOrganisation />}
          />
          <Route
            exact
            path="/organisation/update/:id"
            element={<EditOrganisation />}
          />
          <Route
            exact
            path="/department/readAll"
            element={<AllDepartments />}
          />
          <Route exact path="/department/create" element={<AddDepartment />} />
          <Route
            exact
            path="/department/update/:id"
            element={<EditDepartment />}
          />
         
          <Route exact path="/user/readAll" element={<AllUsers />} />
          <Route exact path="/user/create" element={<AddUser />} />
          <Route exact path="/user/:id" element={<ViewUser />} />
        
          <Route exact path="/user/update/:id" element={<EditUser />} />
          <Route exact path="/user/addUserRoles/:id" element={<AddToUserNewRole />} />
          <Route exact path="/user/removeUserRoles/:id" element={<RemoveRole />} />
          <Route exact path="/role/readAll" element={<AllRoles />} />
          <Route exact path="/role/create" element={<AddRole />} />
          <Route exact path="/role/:id" element={<ViewRole />} />
          <Route exact path="/role/update/:id" element={<EditRole />} />
        </Routes>
         
      </Router>
    </div>
  );
}

export default App;

// import { Height } from "@mui/icons-material"
// import EditIcon from '@mui/icons-material/Edit';
// import { DataGrid } from '@mui/x-data-grid';

// const usersList = [{ id: 1, username: "tosho", fullName: "Todor", age: 29 }];

//   const [time, setTime] = React.useState(new Date());
//   React.useEffect(() => {
//     const interval = setInterval(() => {
//       setTime(new Date());
//     }, 1000);
//     return () => {
//       clearInterval(interval);
//     };
//   }, []);

//   const [data, setData] = useState(usersList);
//   const columns = [
//     { headerName: "ID", field: "id", width: 150},
//     { headerName: "Username", field: "username", width: 150 },
//     { headerName: "Fullname", field: "fullName", width: 150 },
//     { headerName: "Age", field: "age", width: 150 },
//   ];

//   return (
//     <div>
//       <h3 align="center">Time now is: {time.toString()}</h3>
//       <div className="App" style={{height: "500px"}}>
//         <h1 align="center">Users</h1>
//         <h4 align="center">CRUD operation</h4>
//         <DataGrid
//         rows={data}
//         columns={columns}
//         pageSize={5}
//         rowsPerPageOptions={[5]}
//         checkboxSelection
//         disableSelectionOnClick
//         experimentalFeatures={{ newEditingApi: true }}
//       />

//       </div>
//     </div>
//   );
// }
