import * as React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { useNavigate } from "react-router-dom";


export default function Home() {
  const navigate = useNavigate();
  return (
    <ImageList sx={{ width: 1000, height: 887, marginLeft: "44vh", WebkitBoxShadow: "1px 2px 18px 26px rgba(0,0,0,0.75)"}} >
      <ImageListItem key="Subheader" cols={2}></ImageListItem>
      {itemData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=248&fit=crop&auto=format`}
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
          <ImageListItemBar
            title={item.title}
            subtitle={item.author}
            actionIcon={
              <IconButton
                sx={{ color: "rgba(255, 255, 255, 0.8)" }}
                aria-label={`info about ${item.title}`}
                onClick={(e) => {
                  navigate(item.route);
                }}
              >
                <ArrowForwardIcon />
              </IconButton>
            }
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}

const itemData = [
  {
    img: "https://www.volunteeringnz.org.nz/wp-content/uploads/organisation-complex-like-puzzle-pictured-as-word-organisation-puzzle-pieces-to-show-organisation-can-be-difficult-164220275-2.jpg",
    title: "Organisation",
    route: "/organisation/readAll",
    rows: 2,
    cols: 2,
    featured: true
  },
  {
    img: "https://resources.finalsite.net/images/f_auto,q_auto/v1547658480/wyandotteorg/wmbrccwhhcmb2luvn7sx/departments.jpg",
    title: "Departments",
    route: "/department/readAll",
  },
  {
    img: "https://www.azernews.az/media/pictures/managing-users.jpg",
    title: "Users",
    rows: 2,
    cols: 2,
    route: "/user/readAll",
    
  },
  {
    img: "https://www.datascience-pm.com/wp-content/uploads/2020/01/roles2.jpg",
    title: "Roles",
    route: "/role/readAll",
    // author: '@tjdragotta',
  },
];
