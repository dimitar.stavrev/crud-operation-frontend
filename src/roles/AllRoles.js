import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./AllRoles.css"


const axiosConfig = {
    headers: {
      "content-type": "application/json",
    },
    responseType: "json",
    data: {},
  };

export default function AllRoles(){

    const [roles, setRoles] = useState([]);

    useEffect(() => {
       loadRoles();
     }, []);
   
     const loadRoles = async () => {
       const result = await axios.get(
         "http://localhost:8080/role/readAll",
         axiosConfig
       );
       setRoles(result.data);
     };
   
     const deleteRole = async (roleId) => {
       await axios.delete(
         `http://localhost:8080/role/delete/${roleId}`,
         axiosConfig
       );
       loadRoles();
     };

    return (
 <div className="container container-roles mt-4">
        <div className="py-4">
        <h1 className="head-title">
          <Link
            className="btn btn-primary"
            style={{ float: "left" }}
            to={"/role/create"}
          >
            Add Role
          </Link>
          Roles
          <Link
            className="btn btn-danger mx-2"
            style={{ float: "right" }}
            to={"/"}
          >
            Back
          </Link>
        </h1>
          <table className="table border shadow">
            <thead className="titles">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Tag</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody className="body">
              {roles.map((role, index) => (
                <tr key={role.id}>
                  <th scope="row" key={role.id}>
                    {role.id}
                  </th>
                  <td>{role.name}</td>
                  <td>{role.tag}</td>
                  <td>
                    <Link
                      className="btn btn-success mx-2"
                      to={`/role/${role.id}`}
                    >
                      View
                    </Link>
                    <Link
                      className="btn btn-primary mx-2"
                      to={`/role/update/${role.id}`}
                    >
                      Edit
                    </Link>
                    <button
                      className="btn btn-danger mx-2"
                      onClick={() => deleteRole(role.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
}