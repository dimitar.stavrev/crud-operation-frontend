import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./AddUser.css";

export default function AddUsers() {
  let navigate = useNavigate();

  const [user, setUser] = useState({
    username: "",
    password: "",
    fullName: "",
    age: "",
  });

  const [errorsForm, setErrorsForm] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const { username, password, fullName, age } = user;

  const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
    onInputChange(e);
  }

  const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    setErrorsForm(validate(user));
    setIsSubmit(true);
    await axios.post("http://localhost:8080/user/create", user);
    navigate("/user/readAll");
  };

  useEffect(() => {
    console.log(errorsForm);
    if (Object.keys(errorsForm).length === 0 && isSubmit) {
      console.log(user);
    }
  }, [errorsForm]);

  const validate = (values) => {
    const errors = {};
    if (!values.username) {
      errors.username = "Username cannot be empty!";
    } else if (values.username.length < 3) {
      errors.username = "Username must be more than 2 characters!";
    } else if (values.username.length > 20) {
      errors.username = "Username must be less than 20 characters!";
    }
    
    if (!values.password) {
      errors.password = "Password cannot be empty!";
    } else if (values.password.length < 3) {
      errors.password = "Username must be more than 2 characters!";
    } else if (values.password.length > 20) {
      errors.password = "Username must be less than 20 characters!";
    }
    if (!values.fullName) {
      errors.fullName = "Fullname cannot be empty!";
    } else if (values.fullName.length < 5) {
      errors.fullName = "Fullname must be more than 4 characters!";
    }
    else if (values.fullName.length > 30) {
      errors.fullName = "Fullname must be less than 30 characters!";
    }
    if (!values.age) {
      errors.age = "Age cannot be empty!";
    } else if (values.age < 4) {
      errors.age = "Age must be more than 3 years old!";
    }else if(values.age > 120){
      errors.age="Age must be less than 120 years old!"
    }
    return errors;
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Register User</h2>
          <form onSubmit={onSubmit}>
            <div className="mb-3">
              <label htmlFor="Username" className="form-label">
                Username
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your username"
                name="username"
                value={username}
                onChange={onInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.username}</p>
            <div className="mb-3">
              <label htmlFor="Password" className="form-label">
                Password
              </label>
              <input
                type={"password"}
                className="form-control"
                placeholder="Enter your password"
                name="password"
                value={password}
                onChange={onInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.password}</p>
            <div className="mb-3">
              <label htmlFor="Fullname" className="form-label">
                Fullname
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your fullname"
                name="fullName"
                value={fullName}
                onChange={onInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.fullName}</p>
            <div className="mb-3">
              <label htmlFor="Age" className="form-label">
                Age
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your age"
                name="age"
                value={age}
                onChange={handleNumberInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.age}</p>

            <button type="submit" className="btn btn-success">
              Register
            </button>
            <Link className="btn btn-danger mx-2" to={"/user/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
