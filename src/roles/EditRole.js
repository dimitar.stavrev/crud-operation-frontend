import axios from "axios";
import { useEffect, useState } from "react";
import { Link,useNavigate, useParams } from "react-router-dom";

const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

export default function EditRole() {
  const { id } = useParams();

  let navigate = useNavigate();

  const [role, setRole] = useState({
    name: "",
    tag: "",
  });

  const { tag } = role;

  const onInputChange = (e) => {
    setRole({ ...role, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadRole();
  }, [id]);

  
  const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
    onInputChange(e);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:8080/role/update/${id}`, role);
    navigate("/role/readAll");
  };

  const loadRole = async () => {
    const result = await axios.get(
      `http://localhost:8080/role/getId/${id}`,
      axiosConfig
    );
    setRole(result.data);
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Edit Role</h2>
          <form onSubmit={onSubmit}>
            {/* <div className="mb-3">
              <label htmlFor="Name" className="form-label">
                Name
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your name"
                name="name"
                value={name}
                onChange={onInputChange}
              />
            </div> */}
            <div className="mb-3">
              <label htmlFor="Tag" className="form-label">
                Tag
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your tag!"
                name="tag"
                value={tag}
                onChange={handleNumberInputChange}
              />
            </div>
            <button type="submit" className="btn btn-success">
              Edit
            </button>
            <Link className="btn btn-danger mx-2" to={"/role/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
