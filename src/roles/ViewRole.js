import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";



export default function ViewRole() {

    const axiosConfig = {
        headers: {
          "content-type": "application/json",
        },
        responseType: "json",
        data: {},
      };

    const[role,setRole]=useState({
        name:"",
        tag:""
    })

    const {id} = useParams();

    useEffect(()=>{
        loadRole();
    }, [id]);

    const loadRole =async()=>{
        const result= await axios.get(`http://localhost:8080/role/getId/${id}`,axiosConfig);
        setRole(result.data);
    }

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Role Details</h2>

          <div className="card">
            <div className="card-header">
                Role id: {role.id}
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <b>Name: </b>
                  {role.name}
                </li>
                <li className="list-group-item">
                  <b>Tag: </b>
                  {role.tag}
                </li>
                <Link className="btn btn-primary my-2" to={"/role/readAll"}>Back</Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
