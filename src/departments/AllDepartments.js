import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import axios from "axios";
import { DataGrid } from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import { useParams, Link } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { TextField } from "@mui/material";
import ScreenSearchDesktopIcon from "@mui/icons-material/ScreenSearchDesktop";
import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";

const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

export default function AllDepartments() {
  const [departments, setDepartments] = useState([]);

  useEffect(() => {
    loadDepartments();
  }, []);

  const { id } = useParams();

  const loadDepartments = async () => {
    console.log("pappppap");
    const result = await axios.get(
      "http://localhost:8080/department/readAll",
      axiosConfig
    );
    setDepartments(result.data);
  };

  const deleteDepartment = async (id) => {
    await axios.delete(
      `http://localhost:8080/department/delete/${id}`,
      axiosConfig
    );
    loadDepartments();
  };

  const onChange = async (e) => {
    await axios.put(
      `http://localhost:8080/department/update/${id}`,
      departments
    );
  };

  // SEARCH //

  const [departmentFilter, setDepartmentFilter] = useState({
    name: "",
    // employeesCount: "",
    // operation:""
  });

  const { name } = departmentFilter;

  const onSubmit = async (e) => {
    e.preventDefault();
    const result = await axios.post(
      "http://localhost:8080/department/search",
      departmentFilter
    );

    setDepartments(result.data);
  };

  const onInputChange = (e) => {
    setDepartmentFilter({
      ...departmentFilter,
      [e.target.name]: e.target.value,
    });
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "name",
      headerName: "Name",
      width: 200,
      editable: false,
    },
    {
      field: "employeesCount",
      headerName: "Employees count",
      type: "number",
      width: 200,
      editable: false,
      onchange: { onChange },
    },
    {
      field: "organisation",
      headerName: "Organisation id",
      type: "number",
      width: 220,
      editable: false,
      onchange: { onChange },
    },
    {
      field: "action",
      headerName: "Action",
      type: "button",
      width: 300,
      renderCell: (param) => {
        return [
          <Button
            variant="danger"
            startIcon={<DeleteIcon />}
            onClick={() => deleteDepartment(param.id)}
          ></Button>,
          <IconButton component={Link} to={`/department/update/${param.id}`}>
            <EditIcon />
          </IconButton>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "60%",
        marginLeft: "45vh",
        marginTop: "4vh",
        background: "rgba(255,255,255, 0.9)",
        backgroundSize: "",
        paddingBottom: "5vh",
      }}
    >
      <h1>
        <Link
          className="btn btn-primary"
          style={{ float: "left" }}
          to={"/department/create"}
        >
          Add Department
        </Link>
        {"         "}
        Departments
        <Link className="btn btn-danger" style={{ float: "right" }} to={"/"}>
          Back
        </Link>
      </h1>
      <Card
        component="form"
        onSubmit={onSubmit}
        sx={{ float: "left", marginTop: "3vh" }}
      >
        <ScreenSearchDesktopIcon
          sx={{
            float: "left",
            fontSize: "xxx-large",
            marginTop: "3vh",
            marginRight: "-15vh",
            marginLeft: "10vh",
          }}
        />
        <TextField
          id="outlined-name"
          label="Name"
          name="name"
          value={name}
          onChange={onInputChange}
          sx={{ background: "white", marginTop: "8vh", float: "left" }}
        />

        <Button
          type="submit"
          variant="contained"
          color="success"
          sx={{ marginTop: "15vh", marginLeft: "-18vh", float: "left" }}
        >
          Search
        </Button>
      </Card>
      ,
      <DataGrid
        sx={{ fontSize: "x-large", height: "93%" }}
        rows={departments}
        columns={columns}
        pageSize={7}
        rowsPerPageOptions={[7]}
        // checkboxSelection
        // disableSelectionOnClick
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
}
