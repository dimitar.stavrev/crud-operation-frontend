
import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import axios from "axios";
import { DataGrid } from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import { useParams, Link } from "react-router-dom";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from "@mui/material/IconButton";



const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

export default function AllOrganisations() {

  const [organisations, setOrganisations] = useState([]);

  useEffect(() => {
    loadOrganisations();
  }, []);

  const { id } = useParams();

  const loadOrganisations = async () => {
    const result = await axios.get(
      "http://localhost:8080/organisation/readAll",
      axiosConfig
    );
    setOrganisations(result.data);
  };

  const deleteOrganisation = async (id) => {
    await axios.delete(
      `http://localhost:8080/organisation/delete/${id}`,
      axiosConfig
    );
    loadOrganisations();
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "name",
      headerName: "Name",
      width: 200,
      editable: false,
    },
    {
      field: "address",
      headerName: "Address",
      type: "string",
      width: 220,
      // editable: true,
      
    },
    {
      field: "action",
      headerName: "Action",
      type: "button",
      width: 300,
      renderCell: (param) => {
        return [
          <Button
          variant="danger" startIcon={<DeleteIcon />}
            onClick={() =>
              deleteOrganisation(param.id)
            }
          >
          </Button>,
        <IconButton component={Link} to={`/organisation/update/${param.id}`}>
        <EditIcon />
      </IconButton>,
    
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "55%",
        marginLeft: "45vh",
        marginTop: "4vh",
        background: "rgba(255,255,255, 0.9)",
        paddingBottom:"2vh"
      }}
    >
      <h1>
        
      <Link
            className="btn btn-primary"
            style={{ float: "left" }}
            to={"/organisation/create"}
          >
            Add Organisation
          </Link>
        {"         "}
        Organisation
        <Link
            className="btn btn-danger"
            style={{ float: "right" }}
            to={"/"}
          >
            Back
          </Link>
      </h1>
      <DataGrid
        sx={{ fontSize: "x-large", height:"93%" }}
        getRowId={obj => obj.id}
        rows={organisations}
        columns={columns}
        pageSize={7}
        rowsPerPageOptions={[7]}
        // checkboxSelection
        // disableSelectionOnClick
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
}
