import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import "./AddUser.css";

const axiosConfig = {
    headers: {
      "content-type": "application/json",
    },
    responseType: "json",
    data: {},
  };

export default function RemoveRole() {
  const { id } = useParams();

  let navigate = useNavigate();

  const [user, setUser] = useState({
    userId: id,
    roleId: "",
  });

  const { userId, roleId } = user;


  const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();
  }, [id]);

  
  const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
    onInputChange(e);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:8080/user/removeUserRoles/${id}/${roleId}`, user);                                
    navigate("/user/readAll");
  };

  const loadUser = async () => {
    const result = await axios.get(`http://localhost:8080/user/getId/${id}`, axiosConfig);
    setUser(result.data);
  };



  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Remove Role</h2>
          <form onSubmit={onSubmit}>
          
            <div className="mb-3">
              <label htmlFor="User Id" className="form-label">
                User Id
              </label>
              <input
                type={"text"}
                className="form-control"
                name="userId"
                value={userId}
                readonly=""
                // onChange={onInputChange}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Role Id" className="form-label">
                Role Id
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your role id!"
                name="roleId"
                value={roleId}
                onChange={handleNumberInputChange}
              />
            </div>
            <button type="submit" className="btn btn-success">
              Remove
            </button>
            <Link className="btn btn-danger mx-2" to={"/user/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
