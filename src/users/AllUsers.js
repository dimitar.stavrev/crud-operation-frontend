import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./AllUsers.css";

const axiosConfig = {
  headers: {
    "content-type": "application/json",
  },
  responseType: "json",
  data: {},
};

const Operation = {
  default: "Choose age operation",
  EQUAL: "EQUAL",
  LESS_THAN: "LESS_THAN",
  GREATER_THAN: "GREATER_THAN",
};

export default function AllUsers() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get(
      "http://localhost:8080/user/readAll",
      axiosConfig
    );
    setUsers(result.data);
  };

  const deleteUser = async (userId) => {
    await axios.delete(
      `http://localhost:8080/user/delete/${userId}`,
      axiosConfig
    );
    loadUsers();
  };

  // SEARCH ///

  const [userFilter, setUserFilter] = useState({
    username: "",
    age: "",
    ageOperation: "default",
  });

  const { username, age, ageOperation } = userFilter;

  const onSubmit = async (e) => {
    e.preventDefault();
    const result = await axios.post(
      "http://localhost:8080/user/search",
      userFilter
    );

    setUsers(result.data);
  };


  const onInputChange = (e) => {
    setUserFilter({ ...userFilter, [e.target.name]: e.target.value });
  };

  return (
    <div className="container container-users mt-4">
      <div className="py-4">
        <form onSubmit={onSubmit}>
          <input
            type="text"
            placeholder="Username..."
            className="search mx-2"
            name="username"
            value={username}
            onChange={onInputChange}
          />
          <input
            type="text"
            placeholder="Age..."
            className="search mx-2"
            name="age"
            value={age}
            onChange={onInputChange}
          />
          {/* <select onChange={onInputChange} name="ageOperation">
            <option value="" selected>
              Choose age operation
            </option>
            <option name="ageOperation" value={"EQUAL"}>
              EQUAL
            </option>
            <option name="ageOperation" value={"GREATER_THAN"}>
              GREATER_THAN
            </option>
            <option name="ageOperation" value={"LESS_THAN"}>
              LESS_THAN
            </option>
          </select> */}

          <select  value={ageOperation} onChange={onInputChange} name="ageOperation">
            {Object.keys(Operation).map((key) => (
              <option aria-selected="true" key={key} value={key}>
                {Operation[key]}
              </option>
            ))}
          </select>

          <button type="submit" className="btn btn-success mx-2">
            Search
          </button>
        </form>
        <h1 className="head-title">
          <Link
            className="btn btn-primary"
            style={{ float: "left" }}
            to={"/user/create"}
          >
            Add User
          </Link>
          Users
          <Link
            className="btn btn-danger mx-2"
            style={{ float: "right" }}
            to={"/"}
          >
            Back
          </Link>
        </h1>
        <table className="table border shadow">
          <thead className="titles">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Username</th>
              <th scope="col">Fullname</th>
              <th scope="col">Age</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody className="body">
            {users.map((user, index) => (
              <tr key={user.id}>
                <th scope="row" key={user.id}>
                  {user.id}
                </th>
                <td>{user.username}</td>
                <td>{user.fullName}</td>
                <td>{user.age}</td>
                <td>
                  <Link
                    className="btn btn-success mx-2"
                    to={`/user/${user.id}`}
                  >
                    View
                  </Link>
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/user/update/${user.id}`}
                  >
                    Edit
                  </Link>
                  <button
                    className="btn btn-danger mx-2"
                    onClick={() => deleteUser(user.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
