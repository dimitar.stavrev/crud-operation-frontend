import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

export default function ViewUser() {
  const axiosConfig = {
    headers: {
      "content-type": "application/json",
    },
    responseType: "json",
    data: {},
  };

  const [user, setUser] = useState({
    username: "",
    fullName: "",
    age: "",
  });

  const { id } = useParams();

  useEffect(() => {
    loadUser();
  }, [id]);

  const loadUser = async () => {
    const result = await axios.get(
      `http://localhost:8080/user/getId/${id}`,
      axiosConfig
    );
    setUser(result.data);
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">User Details</h2>

          <div className="card">
            <div className="card-header">
              User id: {user.id}
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <b>Username: </b>
                  {user.username}
                </li>
                <li className="list-group-item">
                  <b>Fullname: </b>
                  {user.fullName}
                </li>
                <li className="list-group-item">
                  <b>Age: </b>
                  {user.age}
                </li>
                {/* <li className="list-group-item">
                  <b>Roles: </b>
                  {user.roles}
                </li>*/}
                <Link 
                    className="btn btn-success mx-2 mb-2"
                    to={`/user/addUserRoles/${user.id}`}
                  >
                    AddRole
                  </Link>
                <Link
                  className="btn btn-danger mx-2"
                  to={`/user/removeUserRoles/${user.id}`}
                >
                  RemoveRole
                </Link>
                <Link className="btn btn-primary my-2" to={"/user/readAll"}>
                  Back
                </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
