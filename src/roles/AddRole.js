import axios from "axios";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function AddRole() {
  let navigate = useNavigate();

  const [role, setRole] = useState({
    name: "",
    tag: "",
  });

  const { name, tag } = role;

  const [errorsForm, setErrorForm] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
    onInputChange(e);
  }

  const onInputChange = (e) => {
    setRole({ ...role, [e.target.name]: e.target.value });
  };


  const onSubmit = async (e) => {
    e.preventDefault();
    const errors = validate(role);
    setErrorForm(errors);
    const isValid = Object.keys(errors).length === 0;
    if(isValid === false){
        return;
    }
    setIsSubmit(true);
    await axios.post("http://localhost:8080/role/create", role);
    navigate("/user/readAll");
  };

  const validate = (values) => {
    const errors = {};
    console.log(values)
    if (!values.name) {
      errors.name = "Name cannot be empty!";
    } else if (values.name.length < 3) {
      errors.name = "Name must be more than 3 characters!";
    } else if (values.name.length > 30) {
        errors.name="Name must be less than 30 characters!"
    }
  
    if(!values.tag){
        errors.tag="Tag cannot be empty!"
    }else if(values.tag < 0){
        errors.tag="Tag must be positive number!"
    }
    return errors;
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-4 shadow">
          <h2 className="text-center m-4">Register Role</h2>
          <form onSubmit={onSubmit}>
            <div className="mb-3">
              <label htmlFor="Name" className="form-label">
                Name
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your name!"
                name="name"
                value={name}
                onChange={onInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.name}</p>
            <div className="mb-3">
              <label htmlFor="Tag" className="form-label">
                Tag
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your tag!"
                name="tag"
                value={tag}
                onChange={handleNumberInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.tag}</p>
            <button type="submit" className="btn btn-success">
              Register
            </button>
            <Link className="btn btn-danger mx-2" to={"/role/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
