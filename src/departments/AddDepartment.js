import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";



export default function AddDepartment() {
  let navigate = useNavigate();

  const [department, setDepartment] = useState({
    name: "",
    employeesCount: "",
    organisation: ""
  });

  const { name, employeesCount, organisation, users } = department;

  const [errorsForm, setErrorsForm] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

    const handleNumberInputChange = (e) => {
    e.target.value = e.target.value.replace(/[^0-9]/g, "");
    onInputChange(e);
  };

  const onInputChange = (e) => {
    const target = e.target;
    setDepartment({ ...department, [target.name]: target.value });
  };



  const onSubmit = async (e) => {
    e.preventDefault();
    const errors = validate(department);
    setErrorsForm(errors);
    await axios.post("http://localhost:8080/department/create", department);
    navigate("/department/readAll");
  };


  const validate = (values) => {
    const errors = {};
    console.log(values)
    if (!values.name) {
      errors.name = "Name cannot be empty!";
    } else if (values.name.length < 3) {
      errors.name = "Name must be more than 3 characters!";
    } else if (values.name.length > 30) {
        errors.name="Name must be less than 30 characters!"
    }
   
    if(!values.employeesCount){
        errors.employeesCount="Employees count cannot be empty!"
    }else if(values.employeesCount < 0){
        errors.employeesCount="Employees count must be positive number!"
    }
    return errors;
  };


  return (
    <div className="container  mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3  p-4 mt-4 shadow">
          <h2 className="text-center m-4">Register Department</h2>
          <form onSubmit={onSubmit}>
            <div className="mb-3">
              <label htmlFor="Name" className="form-label">
                Name
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your name!"
                name="name"
                value={name}
                onChange={onInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.name}</p>
            <div className="mb-3">
              <label htmlFor="Employees count" className="form-label">
              Employees count
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your employees count!"
                name="employeesCount"
                value={employeesCount}
                onChange={handleNumberInputChange}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Organisation Id" className="form-label">
              Organisation 
              </label>
              <input
                type={"text"}
                className="form-control"
                placeholder="Enter your organisation with id!"
                name="organisation"
                value={organisation}
                onChange={handleNumberInputChange}
              />
            </div>
            <p className="error-message">{errorsForm.employeesCount}</p>
            <button type="submit" className="btn btn-success">
              Register
            </button>
            <Link className="btn btn-danger mx-2" to={"/department/readAll"}>
              Cancel
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
}
